# Answer: 
#    1. Do you prefer vuejs or reactjs? Why ?
I prefer use vue.js, In terms of performance, Vue is lighter and has several advantages over React, such as rendering time.From a developer point of view, it is easier to use vue for multiple lifecycle and properties in vue.js that are not in react.js and also easier for new developers to understand and learn.
#   2. What complex things have you done in frontend development ?
online signature development and control dynamic role with middleware route guard in vue.js
#   3. why does a UI Developer need to know and understand UX? how far do you understand it?
if from me to explore some theories from UX I think I'm not deep enough, but for my experience getting feedback from users and I'm looking for some theory from UX I think that's my understanding to at least discuss with UI / UX to discuss about design
#   4. Give your analysis results regarding https://taskdev.mile.app/login from the UI / UX side!
From the login design, I think it is very good.
but if based on my analysis there are some inputs.
        - for the content "call us now" maybe it can be placed under the login button for customer service because the content from the login is in the middle so the user's eyes don't detect the content at the top and for the color selection for gray I think it is more suitable to replace it with blue old to be more in sync with the colors of the mileApp
        - To change language, you can make an absolute display in the upper right corner and give a rounded background behind it to make it even cleaner
        - I think the back button is not very important for the user
#    5. Deploy On netlify
Deploy URL: https://cocky-goodall-9b0ded.netlify.app/
#    6. (a)
```
let A = 3;
let B = 5;

[A, B] = [B, A];

console.log(A); // => 5
console.log(B); // => 3
```
#    6. (b)
```
function findNumber(numbers) {
  let n = numbers.length;
  let total = ((n + 2) * (n + 1)) / 2;
  for (let i = 0; i < n; i++) {
    total -= numbers[i];
  }
  return total;
}
console.log(findNumber(numbers));
```

#    6. (c)
```
function findNumber(numbers) {
    const found = []
    numbers.forEach(item => {
        const selected = numbers.find(val => val === item)
        if (selected !== undefined) {
            found.push(selected)
        }
    })
    return found
}
console.log(findNumber(numbers));
```
#    6. (c)
```
function findNumber(numbers) {
    const remake = []
    numbers.forEach((index, item) => {
        const string = item.split('.')
        if (item.length === 0) {
            remake[" + index + "] = item[0]
        }

        if (item.length === 1) {
            remake[" + index + "][" + index + "] = item[1]
        }

        if (item.length === 2) {
            remake[" + index + "][" + index + "][" + index + "] = item[2]
        }
    })
    return remake
}
console.log(findNumber(numbers));
```


# mileapplogin

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
